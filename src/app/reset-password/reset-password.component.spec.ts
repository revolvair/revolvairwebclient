import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { ResetPasswordComponent } from './reset-password.component';
import {UserService} from '../services/user-service/user.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, RouterModule} from '@angular/router';
import {Observable} from 'rxjs/Observable';

class MockActivatedRoute extends ActivatedRoute {
    public params = Observable.of({token: 'abcdef1234'});
}

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;
  let userService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ ResetPasswordComponent ],
        providers: [UserService, { provide: ActivatedRoute, useValue: new MockActivatedRoute() }],
        imports: [HttpClientTestingModule]
    })
    .compileComponents();

    userService = TestBed.get(UserService);
  }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResetPasswordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('initializing component validates token', fakeAsync(() => {
        //Arrange
        spyOn(userService, 'validateResetToken').and.returnValue(Observable.of({valid: true}));
        tick();

        //Act
        component.ngOnInit()

        //Assert
        expect(userService.validateResetToken).toHaveBeenCalled();
    }));

    it('submit() empty password doesn\'t go through', fakeAsync(() => {
        //Arrange
        spyOn(userService, 'validateResetToken').and.returnValue(Observable.of({valid: true}));
        tick();

        //Act
        component.submit("","");
        tick();

        //Assert
        expect(component.message).toBe("Veuillez entrer un nouveau mot de passe.");
    }));

    it('submit() non matching passwords doesn\'t go through', fakeAsync(() => {
        //Arrange
        spyOn(userService, 'validateResetToken').and.returnValue(Observable.of({valid: true}));
        tick();

        //Act
        component.submit("abc","def");
        tick();

        //Assert
        expect(component.message).toBe("Les mots de passe entrés ne correspondent pas.");
    }));

    it('submit() calls service\'s method', fakeAsync(() => {
        //Arrange
        spyOn(userService, 'validateResetToken').and.returnValue(Observable.of({valid: true}));
        spyOn(userService, 'resetPassword').and.returnValue(Observable.of({result: 'success'}));
        tick();

        //Act
        component.submit("abc","abc");
        tick();

        //Assert
        expect(userService.resetPassword).toHaveBeenCalled();
    }));
});
