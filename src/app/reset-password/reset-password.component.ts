import { Component, OnInit } from '@angular/core';
import { UserService} from '../services/user-service/user.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  message : string;
  params : Params;
  token: string;
  showForm : boolean;
  error : boolean;
  success : boolean;

  constructor(private userService : UserService,
              private route: ActivatedRoute) {
        this.error = false;
        this.success = false;
  }

  ngOnInit() {
    this.route.params.subscribe( params => this.params = params);

    // Validate
    this.message = "Chargement...";

    this.token = this.params['token'];

    this.userService.validateResetToken(this.token)
        .subscribe(
            res => {
              this.showForm = res.valid;
              this.error = !res.valid;
              this.message = res.valid ? "" : "Le lien de réinitialiser n'est pas valide ou a expiré.";
            }
        );
  }

  submit(password:string, repeat:string){
    if(password != repeat){
      this.message = "Les mots de passe entrés ne correspondent pas.";
      this.error = true;
      return;
    }
    if(password == ""){
      this.error = true;
      this.message = "Veuillez entrer un nouveau mot de passe.";
      return;
    }
    this.error = false;

    this.message = "Réinitialisation..."

    this.showForm = false;

    this.userService.resetPassword(password, this.token)
        .subscribe(res => {
          if(res.error){
            this.error = true;
            this.message = "Erreur lors de la réinitialisation du mot de passe.";
          }
          if(res.result == "success"){
            this.success = true;
            this.message = "Le mot de passe a bien été réinitialisé!";
          }
        });
  }

}
